﻿using BethanysPieShop.Models;
using System.Collections.Generic;

namespace BethanysPieShop.Views.Shared
{
    public class HomeViewModel
    {
        public IEnumerable<Pie> PiesOfTheWeek { get; set; }
    }
}
